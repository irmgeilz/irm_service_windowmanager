package fr.IRM.WindowManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmWindowManagerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmWindowManagerMsApplication.class, args);
	}

}
